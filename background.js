let target = ['sac', 'chakra', 'mirage', 'nac', 'vpn'],
    targetCount = target.length,
    diffDay = 5;

chrome.browserAction.setBadgeBackgroundColor({color:"#ff6600"});    
chrome.runtime.onMessage.addListener(function(message, callback) {
    if (message === 'setBadge') {
        setBadge();
    }
});

function setBadge()
{
    let data = [];
    let needApprovalCount = 0;
    let currentDate = new Date();
    let tempDate, tempDateObject;

    chrome.storage.sync.get(target, function(googleData) {
        for (let i = 0; i < targetCount; i++) {
            data[target[i]] = googleData[target[i]] || null;

            if (data[target[i]]) {
                tempDate = data[target[i]].split('-');

                if (tempDate.length === 3) {
                    tempDateObject = new Date(tempDate[0], Number(tempDate[1]) - 1, tempDate[2]);

                    if ((tempDateObject.getTime() - currentDate.getTime()) / 1000 / 60 / 60 / 24 <= diffDay) {
                        needApprovalCount++;
                    }
                }
            }
        }
        
        if (needApprovalCount > 0) {
            chrome.browserAction.setBadgeText({ 
                text: "" + needApprovalCount
            });
        } else {
            chrome.browserAction.setBadgeText({ 
                text: ""
            });
        }
    });
}

window.onload = setBadge;

setInterval(function() {
    setBadge();
}, 1000 * 60 * 30);